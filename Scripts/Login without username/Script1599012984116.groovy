import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.demoblaze.com/index.html')

WebUI.click(findTestObject('Page_STORE/a_Log in'))

WebUI.waitForElementPresent(findTestObject('Page_STORE/input_Username_loginusername'), 3)

WebUI.setEncryptedText(findTestObject('Page_STORE/input_Password_loginpassword'), '4nvbrPglk7k=')

WebUI.click(findTestObject('Page_STORE/button_Log in'))

'verifying that alert unsuccess login appear\r\n'
WebUI.verifyAlertPresent(4)

'Displaying error message, and save the response\r\n'
savealert = WebUI.getAlertText()

'Asserting error message, which previously saved'
WebUI.verifyMatch(savealert, 'Please fill out Username.', true)

