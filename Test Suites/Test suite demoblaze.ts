<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suite demoblaze</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>345cbb0c-e4e4-4c91-ae81-669303d7892e</testSuiteGuid>
   <testCaseLink>
      <guid>8b41aa48-07e4-40ce-8292-6d7fd046d068</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login with invalid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d150e7a-31bb-4449-bab1-32974068def1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login with unregistered username</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b167e3e-a517-4f1f-9ea1-bd0d25bf4154</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login with valid credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80c9ac8a-57d7-4563-98a3-f91da7d61ad9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login without filling any credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>502f4602-2f9b-40ba-92b7-f72461085918</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login without password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f20102f8-3592-4344-b9dd-a14b9bf13b93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login without username</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
